#!/bin/bash

# uses checkcite on the auxfile to find unused bib entries and cleans up the bibfile
# outputs the trimmed file to outfile

bibfile="references.bib"
outfile="out.bib"
auxfile="document-3.aux"

# read and clean extra lines and stuff in bib file
entries=$(sed -n '/@/,/^}/p' "$bibfile")

# count and gen list of unused entries
unused_n=$(checkcites --unused "$auxfile" | head -n20 | tail -n1 | cut -d" " -f7)
unused_l=$(checkcites --unused "$auxfile" | tail -n "$unused_n" | cut -d" " -f2)

# create tmpfile to put clean data in
tmpfile=$(mktemp)
trap "rm -f $tmpfile" 0 2 3 15

cleanbib() {
    # catch argument
    local entries="$1"

    # recursive end condition
    if [ -n "$entries" ]; then
        :
    else
        return 0
    fi

    # extract first block
    block=$(echo "$entries" | awk -v n=0 -v m=1 '/^}/{p++} p==m{print; exit} p>=n')

    # if NOT unused append to out_clean.bib
    if [ "$(grep -wc "$unused_l" <<< "$block")" -eq 0 ]; then
        echo "$block" >> "$tmpfile"
    fi

    # delete processed block
    entries=$(tail -n "+""$(($(wc -l <<< "$block")+1))" <<< "$entries")

    # recursive call
    cleanbib "$entries"
}

cleanbib "$entries"

# remove ^m 
cat "$tmpfile" | tr '\r' ' ' > "$outfile"
