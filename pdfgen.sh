#!/usr/bin/env bash
set -euo pipefail

# Batch converts all svgs to pdfs.
# Will create fn.pdf one level above the location of svg
# Written considering a directory structure such as "latexproj_root/figure/chap1/originals/picture1.svg"

cd "$(pwd)"
files=$(fd --regex '.*\.svg')
for f in $files
do
    fn=$(basename "$f" .svg)
    dirn=$(dirname "$(dirname "$f")")
    ofn="$dirn"/"$fn".pdf
    echo "$ofn"
    sem -j+0 inkscape --export-area-page --export-type=pdf --export-filename="$ofn" "$f" > /dev/null 2>&1
done
sem --wait
