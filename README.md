Scripts that help deal with latex writing. 

# cleanbib.sh
Trims bib file for unused entries and cleans up the file in general.
Special depends: checkcites (part of texlive-core in the archlinux repo)

# downloadrefs.sh
Downloads all the entries off scihub in a bib file with DOIs.
Special depends: scholarref tools (https://adamsgaard.dk/scholarref.html)

# pdfgen.sh
Converts all svgs in latex project folder to pdf.
Will place files in folder one level above the svg file's location.
Written considering a directory structure such as:
"latexproj_root/figure/chap1/originals/picture1.svg"
