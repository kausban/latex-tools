#!/bin/bash

bibfile="BIBfile.bib"

grep -E "doi.+=" "$bibfile" | awk -F"[{}]" '{print $2}' | while read -r line ; do
    echo ""
    echo "################"
    echo "Processing $line"
    echo "################"

    shdl "$line"
    
    read -p "Hit enter to continue" </dev/tty
done
